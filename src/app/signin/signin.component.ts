import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, NgForm } from '@angular/forms';
import { AuthUser } from '../api/auth-user';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  userForm = new FormGroup({
    email: new FormControl(),
    lastName: new FormControl(),
  });
  

  
  constructor(  ) { }

  ngOnInit() {
  }

  onSubmit(f: NgForm) {
    // TODO: Use EventEmitter with form value
    console.log(f.value.email);
    var user = (AuthUser('danieltp123@gmail.com','123456789'));
    console.log(user)
  }

}
