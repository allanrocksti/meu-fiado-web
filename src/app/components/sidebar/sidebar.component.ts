import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard',    title: 'Dashboard',             icon: 'dashboard',  class: '' },
    { path: '/list-clients', title: 'Clientes',              icon: 'person',     class: '' },
    { path: '/registration', title: 'Cadastros',             icon: 'person_add', class: '' },
    { path: '/add-release',  title: 'Adicionar lançamento',  icon: 'add',        class: 'active-pro' },
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
    menuItems: any[];

    constructor() { }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
}
