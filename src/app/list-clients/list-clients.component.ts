import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';

@Component({
    selector: 'app-list-clients',
    templateUrl: './list-clients.component.html',
    styleUrls: ['./list-clients.component.css']
})
export class ListClientsComponent implements OnInit {

    users = [];

    constructor(private _router: Router) { }

    ngOnInit() {

        this.users.push({
            'Name': 'Allan Roque Barbosa da Silva',
            'CPF': '017.854.664-02',
            'Value': 400.00
        });
        this.users.push({
            'Name': 'Allan Roque Barbosa da Silva',
            'CPF': '017.854.664-02',
            'Value': 400.00
        });
        this.users.push({
            'Name': 'Allan Roque Barbosa da Silva',
            'CPF': '017.854.664-02',
            'Value': 400.00
        });
        this.users.push({
            'Name': 'Allan Roque Barbosa da Silva',
            'CPF': '017.854.664-02',
            'Value': 400.00
        });
    }

    isMobileMenu() {
        if (window.screen.width > 991) {
            return false;
        }
        return true;
    };

    navigateToReleases(user) {
        this._router.navigate(['/releases'], this.createNavigationExtras(user));
    }

    navigateToUpdateClient(user) {
        this._router.navigate(['/registration/client'], this.createNavigationExtras(user));
    }

    createNavigationExtras(user): NavigationExtras {
        return {
            queryParams: {
                'Name': user['Name'],
                'CPF': user['CPF'],
                'Value': user['Value']
            }
        };
    }

    showModalDeleteClient() {
        $('#deleteClientModal').modal();
    }

    deleteClient(user) {
        console.log(user);
    }

}
