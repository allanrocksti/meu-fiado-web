import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-register-client',
    templateUrl: './register-client.component.html',
    styleUrls: ['./register-client.component.scss']
})
export class RegisterClientComponent implements OnInit {

    editClient = false;
    clientWithoutLogin = false;
    noLimitValue = false;
    user: any;

    constructor(private _activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this._activatedRoute.queryParams
            .subscribe(params => this.user = params)

        if (this.user['Name'] !== undefined) { this.editClient = true; }
    }

}
