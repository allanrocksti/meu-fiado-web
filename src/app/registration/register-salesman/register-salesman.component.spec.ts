import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterSalesmanComponent } from './register-salesman.component';

describe('RegisterSalesmanComponent', () => {
  let component: RegisterSalesmanComponent;
  let fixture: ComponentFixture<RegisterSalesmanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterSalesmanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterSalesmanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
