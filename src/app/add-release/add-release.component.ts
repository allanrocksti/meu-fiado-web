import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-add-release',
    templateUrl: './add-release.component.html',
    styleUrls: ['./add-release.component.css']
})
export class AddReleaseComponent implements OnInit {

    users = [];

    constructor() { }

    ngOnInit() {

        this.users.push({
            'id': 1,
            'name': 'Allan Roque'
        });
        this.users.push({
            'id': 2,
            'name': 'Alex Roque'
        });
        this.users.push({
            'id': 3,
            'name': 'Lucia Roque'
        });
        this.users.push({
            'id': 4,
            'name': 'Aldo Barbosa'
        });
        this.users.push({
            'id': 5,
            'name': 'Jackson Terceiro'
        });

    }

    addRelease() {

        // no pc: left, no smart: center

        $.notify({
            icon: 'settings',
            message: 'Ocorreu algum erro ao lançar, verifique os campos e tente novamente...'
        },
        {
            type: 'danger',
            timer: 1000,
            placement: {
                from: 'top',
                align: 'center'
            }
        });

        $.notify({
            icon: 'settings',
            message: 'Lançado com sucesso'
        },
        {
            type: 'success',
            timer: 1000,
            placement: {
                from: 'top',
                align: 'center'
            }
        });
    }

}
