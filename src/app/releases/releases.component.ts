import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';

@Component({
    selector: 'app-releases',
    templateUrl: './releases.component.html',
    styleUrls: ['./releases.component.scss']
})
export class ReleasesComponent implements OnInit {

    user: any;
    releases = [];
    totalValue = 0;

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _router: Router
    ) { }

    ngOnInit() {
        this._activatedRoute.queryParams
            .subscribe(params => this.user = params)

        this.releases.push({
            'checked': false,
            'id': 101,
            'value': 100.00,
            'date': '1995-12-17',
            'description': 'Compras'
        });
        this.releases.push({
            'checked': false,
            'id': 102,
            'value': 100.00,
            'date': '1995-12-17',
            'description': 'Coca cola'
        });
        this.releases.push({
            'checked': false,
            'id': 103,
            'value': 100.00,
            'date': '1995-12-17',
            'description': 'vaca'
        });
        this.releases.push({
            'checked': false,
            'id': 104,
            'value': 100.00,
            'date': '1995-12-17',
            'description': 'boi'
        });
        this.releases.push({
            'checked': false,
            'id': 105,
            'value': 100.00,
            'date': '1995-12-17',
            'description': 'galinha super master hiper mega boladona das galaxias interlactiais'
        });

        this.releases.forEach(element => {
            this.totalValue += element['value'];
        });
    }

    isMobileMenu() {
        if (window.screen.width > 991) {
            return false;
        }
        return true;
    };

    navigateToMakePayment() {
        this._router.navigate(['/releases/make-payment'], this.createNavigationExtras());
    }

    createNavigationExtras(): NavigationExtras {
        return {
            queryParams: {
                'Name': this.user['Name'],
                'CPF': this.user['CPF'],
                'Value': this.user['Value']
            }
        };
    }

}
