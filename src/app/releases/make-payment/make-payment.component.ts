import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-make-payment',
    templateUrl: './make-payment.component.html',
    styleUrls: ['./make-payment.component.scss']
})
export class MakePaymentComponent implements OnInit {

    user: any;
    releases = [];
    checkedReleases = [];

    constructor(private _activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this._activatedRoute.queryParams
            .subscribe(params => this.user = params)

        this.releases.push({
            'checked': false,
            'id': 101,
            'value': 100.00,
            'date': '1995-12-17',
            'description': 'Compras'
        });
        this.releases.push({
            'checked': false,
            'id': 102,
            'value': 100.00,
            'date': '1995-12-17',
            'description': 'Coca cola'
        });
        this.releases.push({
            'checked': false,
            'id': 103,
            'value': 100.00,
            'date': '1995-12-17',
            'description': 'vaca'
        });
        this.releases.push({
            'checked': false,
            'id': 104,
            'value': 100.00,
            'date': '1995-12-17',
            'description': 'boi'
        });
        this.releases.push({
            'checked': false,
            'id': 105,
            'value': 100.00,
            'date': '1995-12-17',
            'description': 'galinha super master hiper mega boladona das galaxias interlactiais'
        });

    }

    checkRelease(args, release) {
        const index: number = this.releases.indexOf(release)
        this.releases[index]['checked'] = args.target.checked;
        this.addOrRemoveRelease(release);
    }

    checkReleases(args) {
        this.releases.forEach(element => {
            if (element['checked'] !== args.target.checked) {
                element['checked'] = args.target.checked;
                this.addOrRemoveRelease(element);
            }
        });
    }

    addOrRemoveRelease(release) {
        if (release['checked']) {
            this.checkedReleases.push(release);
        } else {
            const index: number = this.checkedReleases.indexOf(release);
            if (index !== -1) {
                this.checkedReleases.splice(index, 1);
            }
        }
    }

    sumValueCheckedReleases() {
        let amount = 0;

        this.checkedReleases.forEach(element => {
            amount += element['value'];
        });

        return amount;

    }

    isMobileMenu() {
        if (window.screen.width > 991) {
            return false;
        }
        return true;
    };

}
