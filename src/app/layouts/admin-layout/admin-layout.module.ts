import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { NgxMaskModule } from 'ngx-mask'

import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule
} from '@angular/material';
import { RegistrationComponent } from 'app/registration/registration.component';
import { RegisterClientComponent } from 'app/registration/register-client/register-client.component';
import { RegisterSalesmanComponent } from 'app/registration/register-salesman/register-salesman.component';
import { AddReleaseComponent } from 'app/add-release/add-release.component';
import { ListClientsComponent } from 'app/list-clients/list-clients.component';
import { ReleasesComponent } from 'app/releases/releases.component';
import { MakePaymentComponent } from 'app/releases/make-payment/make-payment.component';
import { NgSelectModule } from '@ng-select/ng-select'
import { PageNotFoundComponent } from 'app/page-not-found/page-not-found.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    NgxMaskModule.forRoot(),
    NgSelectModule
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    ListClientsComponent,
    RegistrationComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    AddReleaseComponent,
    RegisterClientComponent,
    RegisterSalesmanComponent,
    ReleasesComponent,
    MakePaymentComponent,
    PageNotFoundComponent
  ]
})

export class AdminLayoutModule {}
