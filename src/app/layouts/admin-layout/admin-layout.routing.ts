import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { RegistrationComponent } from 'app/registration/registration.component';
import { RegisterClientComponent } from 'app/registration/register-client/register-client.component';
import { RegisterSalesmanComponent } from 'app/registration/register-salesman/register-salesman.component';
import { AddReleaseComponent } from 'app/add-release/add-release.component';
import { ListClientsComponent } from 'app/list-clients/list-clients.component';
import { ReleasesComponent } from 'app/releases/releases.component';
import { MakePaymentComponent } from 'app/releases/make-payment/make-payment.component';
import { PageNotFoundComponent } from 'app/page-not-found/page-not-found.component';

export const AdminLayoutRoutes: Routes = [
    // {
    //   path: '',
    //   children: [ {
    //     path: 'dashboard',
    //     component: DashboardComponent
    // }]}, {
    // path: '',
    // children: [ {
    //   path: 'userprofile',
    //   component: UserProfileComponent
    // }]
    // }, {
    //   path: '',
    //   children: [ {
    //     path: 'icons',
    //     component: IconsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'notifications',
    //         component: NotificationsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'maps',
    //         component: MapsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'typography',
    //         component: TypographyComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'upgrade',
    //         component: UpgradeComponent
    //     }]
    // }
    { path: 'dashboard',                    component: DashboardComponent },
    { path: 'user-profile',                 component: UserProfileComponent },
    { path: 'list-clients',                 component: ListClientsComponent },
    { path: 'registration',                 component: RegistrationComponent },
    { path: 'icons',                        component: IconsComponent },
    { path: 'maps',                         component: MapsComponent },
    { path: 'notifications',                component: NotificationsComponent },
    { path: 'add-release',                  component: AddReleaseComponent },
    { path: 'registration/client',          component: RegisterClientComponent },
    { path: 'registration/salesman',        component: RegisterSalesmanComponent },
    { path: 'releases',                     component: ReleasesComponent },
    { path: 'releases/make-payment',        component: MakePaymentComponent },
    { path: '**',                           component: PageNotFoundComponent },
];
